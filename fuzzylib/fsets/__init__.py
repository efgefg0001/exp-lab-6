__author__ = ''

__all__ = ["fuzzy_set", "gamma", "gaussian", "pifunction", "polygon",
           "polynomial", "sfunction", "singleton", "trapezoid", "triangular",
           "zfunction"
           ]
