from fuzzylib.adjective import Adjective
from fuzzylib.fsets.trapezoid import Trapezoid
from fuzzylib.ruleblock import RuleBlock
from fuzzylib.systems.mamdani import MamdaniSystem
from fuzzylib.systems.sugeno import SugenoSystem
from fuzzylib.variable import Variable


def mamamdani_test():
    # --- Variables ---
    print("\n\n\n")

    cold = Adjective('cold', Trapezoid((-100, 0), (-50, 1), (-10, 1), (14, 0)))
    normal = Adjective('normal', Trapezoid((15, 0), (20, 1), (30, 1), (35, 0)))
    hot = Adjective('hot', Trapezoid((36, 0), (50, 1), (60, 1), (65, 0)))

    small = Adjective('small', Trapezoid((-100, 0), (-50, 1), (-10, 1), (14, 0)))
    middle = Adjective('middle', Trapezoid((15, 0), (20, 1), (30, 1), (35, 0)))
    long = Adjective('long', Trapezoid((36, 0), (50, 1), (60, 1), (65, 0)))

    temp = Variable('temp', '°C', cold, normal, hot)
    path = Variable('path', 'm', small, middle, long)

    short = Adjective('short', Trapezoid((0, 1), (10, 1), (12, 1), (14, 0)))
    usual = Adjective('usual', Trapezoid((10, 0), (15, 1), (25, 1), (30, 0)))
    large = Adjective('large', Trapezoid((31, 0), (36, 1), (40, 1), (45, 0)))

    z = Variable('z', 'min', short, usual, large, defuzzification='COG', default=0)

    scope = locals()

    rule1 = 'if temp is cold and  path is long then z is short'
    rule2 = 'if temp is normal and path is middle then z is usual'
    rule3 = 'if temp is hot and path is long then z is short'

    inputs = {'temp': -5, "path": 50}

    block = RuleBlock('first', activation='MIN', accumulation='MAX')
    block.add_rules(rule1, rule2, rule3, scope=scope)

    mamdani = MamdaniSystem('ms1', block)
    res = mamdani.compute(inputs)
    print("mamdani result = {}".format(list(res.values())[0]))


def sugeno_test():
    print("\n\n\n")

    cold = Adjective('cold', Trapezoid((-100, 0), (-50, 1), (-10, 1), (14, 0)))
    normal = Adjective('normal', Trapezoid((15, 0), (20, 1), (30, 1), (35, 0)))
    hot = Adjective('hot', Trapezoid((36, 0), (50, 1), (60, 1), (65, 0)))

    small = Adjective('small', Trapezoid((-100, 0), (-50, 1), (-10, 1), (14, 0)))
    middle = Adjective('middle', Trapezoid((15, 0), (20, 1), (30, 1), (35, 0)))
    long = Adjective('long', Trapezoid((36, 0), (50, 1), (60, 1), (65, 0)))

    path = Variable('path', 'm', small, middle, long)
    temp = Variable('temp', '°C', cold, normal, hot)

    rule4 = 'if temp is cold and path is long then z=7'
    rule5 = 'if temp is normal and path is middle then z=15'
    rule6 = 'if temp is hot and path is long then z=8'

    block = RuleBlock('second', activation='MIN')
    scope = locals()
    block.add_rules(rule4, rule5, rule6, scope=scope)
    inputs = {'temp': -5, "path": 50}
    sugeno = SugenoSystem('ss1', block)
    res = sugeno.compute(inputs)

    print(f"sugeno result = {list(res.values())}")


if __name__ == "__main__":
    mamamdani_test()
    sugeno_test()
