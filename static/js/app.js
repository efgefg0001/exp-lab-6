//// async function
//async function fetchAsync() {
//    const url = "http://api.icndb.com/jokes/random?firstName=John&amp;lastName=Doe";
//    // await response of fetch call
//    let response = await fetch(url);
//    // only proceed once promise is resolved
//    let data = await response.json();
//    // only proceed once second promise is resolved
//    return data;
//}
//
//// trigger async function
//// log response or catch error of fetch promise
//fetchAsync()
//    .then(data => console.log(`got joke = ${JSON.stringify(data)}`))
//    .catch(reason => console.log(reason.message));

const headers = new Headers({
    "Content-Type": "application/json; charset=utf-8"
});

Vue.component("in-variable", {
    props: {
        dddata: Object,
        vvvariableValue: Number
    },
    template: `
<div class="card">
    <h4 class="card-header">{{ dddata.variable.label }} ({{dddata.key}}):</h4> 
    <div class="card-body">
        <input  v-model="vvvariableValue" v-on:input="updateValue($event.target.value)">
        <button class="btn btn-primary" @click="needDraw"><span class="fa fa-asterisk"></span></button>
    </div>
</div>`,
    mounted: function () {
        this.vvvariableValue = localStorage.getItem(this.dddata.key);
    },
    methods: {

        needDraw: function () {
            console.log("emit");
            app.$emit("needDrawPlot", this.dddata.variable);
        },

        updateValue: function (value) {
            localStorage.setItem("showResults", "false");
            localStorage.setItem(this.dddata.key, "" + value);
        }
    }

});

Vue.component("out-variable", {
    props: {
        dddata: Object,
        vvvariableValue: String
    },
    template: `
<div class="card">
    <h4 class="card-header">{{ dddata.variable.label }} ({{dddata.key}}):</h4>
    <div class="card-body">
     {{ vvvariableValue }} 
        <button class="btn btn-primary" @click="needDraw"><span class="fa fa-asterisk "></span></button>     
     </div>
</div>`,
    mounted: function () {
        const showResults = localStorage.getItem("showResults");
        if (showResults && showResults === "true")
            this.vvvariableValue = localStorage.getItem(this.dddata.key);
        else
            this.vvvariableValue = "";

    },
    methods: {
        needDraw: function () {
            console.log("emit");
            app.$emit("needDrawPlot", this.dddata.variable);
        }
    }
});

const app = new Vue({
    el: '#app',
    data: {
        system: {},
        inputVars: [],
        outputVars: [],
        outputVarsKeyIndexMap: [],
        showModal: false,
        msg: "EMPTY"
    },
    mounted: function () {

        fetch("/api/system", {headers: headers})
            .then(function (response) {
                return response.json();
            })
            .then(function (system) {
                app.system = system;

                for (let key in system.inputVars) {
                    app.inputVars.push({key: key, variable: system.inputVars[key]})
                }
                let i = 0;
                for (let key in system.outputVars) {
                    system.outputVars[key].computedValue = -1000;
                    app.outputVars.push({key: key, variable: system.outputVars[key]})
                    app.outputVarsKeyIndexMap[key] = i;
                    ++i;
                }

                app.msg = JSON.stringify(system);
            })
            .catch(function (err) {
                console.log("error " + err);
            });
    },
    methods: {
        drawThisPlot: function (variable) {
            console.log("catch");

            app.showModal = true;
            const data = [];
            for (let normKey of variable.norms) {
                const norm = app.system.norms[normKey];
                const trace = {
                    x: norm.values,
                    y: [0, 1, 1, 0],
                    type: 'scatter',
                    name: normKey
                };
                data.push(trace);
            }
            Plotly.newPlot('plot-surface', data);
        },
        solve: function () {
            const inputVarsValues = {};
            for (let inputVar of this.inputVars) {
                inputVarsValues[inputVar.key] = +localStorage.getItem(inputVar.key);
            }
            const requestBody = JSON.stringify(inputVarsValues);
            fetch("/api/inference", {headers: headers, method: "POST", body: requestBody})
                .then(function (response) {
                    return response.json();
                })
                .then(function (solution) {
                    for (let key in solution) {
                        const index = app.outputVarsKeyIndexMap[key];
                        app.outputVars[index].variable.computedValue = solution[key];
                        localStorage.setItem(key, solution[key]);
                    }
                    console.log(`got solution ${JSON.stringify(solution)}`);
                    localStorage.setItem("showResults", "true");
                    window.location.reload(true);
                })
                .catch(function (err) {
                    console.log("error " + err);
                });

            console.log(`ready for send ${JSON.stringify(inputVarsValues)}`);
        }
    }
});

app.$on("needDrawPlot", function (variable) {
    console.log(`catch event = ${JSON.stringify(variable)}`);
    app.drawThisPlot(variable);

    console.log("kokoko");
});

var modallll = Vue.component('modal', {
    template: '#modal-template'
});
