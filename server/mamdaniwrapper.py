#!/usr/bin/env python3

import json

from fuzzylib.adjective import Adjective
from fuzzylib.fsets.trapezoid import Trapezoid
from fuzzylib.variable import Variable
from fuzzylib.ruleblock import RuleBlock
from fuzzylib.systems.mamdani import MamdaniSystem

from .props import SystemProp, NormType


class MamdaniWrapper:

    def __init__(self, path = None):
        if path is not None:
            self.load(path)
        else:
            self.system = None

    def load(self, path):
        with open(path) as fin:
            self.system = json.load(fin)

    def inference(self, inputValues):
        inferenceArgs = {}
        for key, norm in self.system[SystemProp.NORMS].items():
            if norm[SystemProp.TYPE] == NormType.TRAPEZOID:
                a, b, c, d = norm[SystemProp.VALUES]
                inferenceArgs[key] = Adjective(key, Trapezoid((a, 0), (b, 1), (c, 1), (d, 0)))
        for key, inputVar in self.system[SystemProp.INPUT_VARS].items():
            short, usual, large = [inferenceArgs[normName] for normName in inputVar[SystemProp.NORMS]]
            inferenceArgs[key] = Variable(key, inputVar[SystemProp.LABEL], short, usual, large)

        for key, outputVar in self.system[SystemProp.OUTPUT_VARS].items():
            short, usual, large = [inferenceArgs[normName] for normName in outputVar[SystemProp.NORMS]]
            inferenceArgs[key] = Variable(key, "min", short, usual, large, defuzzification='COG', default=0)

        for i, rule in enumerate(self.system[SystemProp.RULES]):
            inferenceArgs[f"rule_${i}"] = rule

        block =  RuleBlock('ruleBlock', activation='MIN', accumulation='MAX')
        block.add_rules(*self.system[SystemProp.RULES], scope=inferenceArgs)

        mamdani = MamdaniSystem('mamdaniSystem', block)
        result = mamdani.compute(inputValues)[block.name]
        print(f"result = {result}")

        return result

