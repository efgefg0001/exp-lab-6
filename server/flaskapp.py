#!/usr/bin/env python3
import json

from flask import Flask, send_file, Response, request

from .mamdaniwrapper import MamdaniWrapper

from .props import ConfProp

STATIC_FOLDER = "../static"


def init_app(inp_config):
    global config
    config = inp_config
    global mamdani_system
    mamdani_system = MamdaniWrapper(config[ConfProp.PATH_TO_DATA])



app = Flask(__name__, static_url_path="/static", static_folder=f"{STATIC_FOLDER}")


@app.route("/")
def index():
    return send_file(f"{STATIC_FOLDER}/index.html")

@app.route("/api/system")
def get_system():
    global mamdani_system
    system = mamdani_system.system
    return json_resp_from_objs(system)

@app.route("/api/inference", methods=["POST"])
def inference():
    input_json = request.get_json(force=True)
    print(f"got json {input_json}")

    global mamdani_system
    solution = mamdani_system.inference(input_json)

    return json_resp_from_objs(solution)

def json_resp_from_objs(objects, status=200):
    content = json.dumps(objects)
    return Response(content, mimetype="application/json", status=status)
