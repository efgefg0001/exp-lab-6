# /usr/bin/env python3

class ConfProp:
    HOST = "host"
    PORT = "port"
    PATH_TO_DATA = "pathToData"

class SystemProp:
    NORMS = "norms"
    TYPE = "type"
    VALUES = "values"
    INPUT_VARS = "inputVars"
    OUTPUT_VARS = "outputVars"
    LABEL = "label"
    RULES = "rules"

class NormType:
    TRAPEZOID = "trapezoid"
