#!/usr/bin/env python3

import unittest

from fuzzylib.adjective import Adjective
from fuzzylib.fsets.trapezoid import Trapezoid
from fuzzylib.rules.rule import Rule
from server.mamdaniwrapper import MamdaniWrapper
from server.props import SystemProp


class TestMamdaniWrapper(unittest.TestCase):

    def test_system_is_None_after_empty_constructor(self):
        wrapper = MamdaniWrapper()
        self.assertIsNone(wrapper.system)

    def test_load_valid_json_to_system(self):
        path = "./data/data.json"
        wrapper = MamdaniWrapper(path)
        system: dict = wrapper.system
        self.assertIsNotNone(system)
        self.assertIsNotNone(system.get(SystemProp.RULES, None))
        self.assertIsNotNone(system.get(SystemProp.OUTPUT_VARS, None))
        self.assertIsNotNone(system.get(SystemProp.INPUT_VARS, None))
        self.assertIsNotNone(system.get(SystemProp.NORMS, None))

    def test_inference(self):
        path = "./data/data.json"
        wrapper = MamdaniWrapper(path)
        result = wrapper.inference({"level": 2.5, "consumption": 0.4})
        self.assertTrue(result is not None)
        self.assertTrue(abs(result["influx"] - 0.437229) < 0.01)


class TestTrapezoid(unittest.TestCase):

    def test_constructor_successful_creation(self):
        func = Trapezoid((3, 0), (5, 1), (7, 1), (8, 0))
        self.assertEqual(str(func), "(3, 0) (5, 1) (7, 1) (8, 0)")

    def test_high(self):
        func = Trapezoid((3, 0), (5, 1), (7, 1), (8, 0))
        memb = func.membership(6)
        self.assertTrue(abs(memb - 1) < 0.01)

    def test_membership_low(self):
        func = Trapezoid((3, 0), (5, 1), (7, 1), (9, 0))
        memb = func.membership(3.1)
        self.assertTrue(abs(memb - 0.05) < 0.01)


class TestsAdjective(unittest.TestCase):

    def test_name(self):
        adjective = Adjective("some_membership_func", Trapezoid((3, 0), (5, 1), (7, 1), (9, 0)))
        name = str(adjective)
        self.assertEqual(name, "some_membership_func")

    def test_repr(self):
        adjective = Adjective("some_membership_func", Trapezoid((3, 0), (5, 1), (7, 1), (9, 0)))
        str_repr = repr(adjective)
        self.assertEqual(str_repr, "Adjective some_membership_func:(3, 0) (5, 1) (7, 1) (9, 0)")

    def test_membership_high(self):
        adjective = Adjective("some_membership_func", Trapezoid((3, 0), (5, 1), (7, 1), (9, 0)))
        memb = adjective.membership(6)
        self.assertTrue(abs(memb - 1) < 0.01)

    def test_membership_low(self):
        adjective = Adjective("some_membership_func", Trapezoid((3, 0), (5, 1), (7, 1), (9, 0)))
        memb = adjective.membership(3.1)
        self.assertTrue(abs(memb - 0.05) < 0.01)

if __name__ == "__main__":
    unittest.main(verbosity=2)
