# /usr/bin/env python3

import argparse
import yaml

from server import flaskapp
from server.props import *


def create_argparser():
    argument_parser = argparse.ArgumentParser(description="Script for viewing oms catalog")
    argument_parser.add_argument("config", type=argparse.FileType("r"), help="path to config file")
    return argument_parser.parse_args()


if __name__ == "__main__":

    args = create_argparser()

    config = yaml.load(args.config)
    flaskapp.init_app(config)

    flaskapp.app.run(flaskapp.config[ConfProp.HOST], flaskapp.config[ConfProp.PORT])
    print("end")
